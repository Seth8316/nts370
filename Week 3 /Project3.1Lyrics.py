import time #allows use of time.sleep()
print "Do I wanna Know? - By Artic monkeys (Without Chorus Lyrics\n"	#title and artist and skips a line
time.sleep(2)		#sleeps for two seconds


#These varibles are the verses split into two catagories V which is the repeated Verses and UV which is not the repeated verse
UV1="Have you got color in your cheeks?\nDo you ever get that fear that you can't shift the type\nThat sticks around like summat in your teeth?\nAre there some aces up your sleeve?\nHave you no idea that you're in deep?\nI've dreamt about you nearly every night this week\nHow many secrets can you keep?\n'Cause there's this tune I found\nThat makes me think of you somehow and I play it on repeat\nUntil I fall asleep, spillin' drinks on my settee\n"
V1 = "If this feelin' flows both ways?\nWas sorta hopin' that you'd stay\nThat the nights were mainly made\nFor sayin' things that you can't say tomorrow day\n"
V2= "Crawlin' back to you\nEver thought of callin' when\nYou've had a few?\n'Cause I always do\nMaybe I'm too\nBusy bein' yours\nTo fall for somebody new\nNow, I've thought it through\nCrawlin' back to you\n"
UV2= "So have you got the guts?\nBeen wonderin' if your heart's still open\nAnd if so, I wanna know what time it shuts\nSimmer down an' pucker up, I'm sorry to interrupt\nIt's just I'm constantly on the cusp of tryin' to kiss you\nI don't know if you feel the same as I do\nBut we could be together if you wanted to\n"
EV = "Too busy bein' yours to fall\nEver thought of callin', darlin'?\nDo you want me crawlin' back to you?"

#These lines print out the entire song while skipping lines and waiting two seconds between each verse

print UV1 

time.sleep(2)

print V1

time.sleep(2)

print V2

time.sleep(2)

print UV2

time.sleep(2)

print V1

time.sleep(2)

print V2

time.sleep(2)

print V1, EV

print "END" 

