#!/bin/bash


function parse1() {
        awk '/authentication failure/' /var/log/auth.log;
};

function parse2() {
        awk '/ubuntu sudo: seth8316/' /var/log/auth.log;
};

function parse3() {
	read -p "What would you like the file to be called? " file
        parse1 >> $file;
        parse2 >> $file;
};

function start() {
        echo "Options are a - d they will show only failed logins, only sudo commands used, both, or both and print them to a log.txt"
	read -p "Which one to do? " a
	if [ $a = 'a' ] || [ $a = 'A' ] ; then
                parse1
	elif [ $a = 'b' ] || [ $a = 'B' ] ; then
                parse2
        elif  [ $a = 'c' ] || [ $a = 'C' ] ; then 
                parse1
		parse2
        elif [ $a = 'd' ] || [ $a = 'D' ] ; then 
                parse3
        elif [ $a = 'exit' ] || [ $a = 'stop' ] ; then 
		echo "Script Exiting" 
	fi

};

start




