#!/bin/bash

#The awk command gets the 3 fields from users in the passwd text file and puts them in a file
#OFS os a seperator for the fields
# $1  is the user name, $6 is the home directory $7 is the command shell
awk -F: '$7 == "/bin/bash" {OFS= ", "}  {print $1 , $6 , $7 }' /etc/passwd > output.txt 

#sed cleans up the unnecessary fields like root and others. Every line after will be a user that was created
sed -i '1,40d' output.txt

#sort organizes the fields with , into alphabetical order
sort -t, output.txt > Organized.txt 


