#!/bin/bash

echo "Only numbers between 1-255 must be entered, NMAP will not be executed if a different value is added"

sleep 5 

#number input command 
read -p 'Starting:  ' start
read -p 'Ending:    '   end

#Number check value command if it is ok  NMAP will proceed 
if [ "$start" -ge 1 -a "$start" -le 255 -a "$end" -le 255 -a "$end" -ge 1 ]; then nmap -oN IPScan.txt 192.168.1.$start-$end;fi 


