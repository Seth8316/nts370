#!/usr/bin/perl
#Interpreter line
use LWP::UserAgent;
#This line seems to enable implementation of web requests
use HTML::Parse;
$site = @ARGV[0];
$filetype = @ARGV[1];
$searchurl = "http://www.google.com/search?hl=en&q=site%3A$site+filetype%3A$filetype";
$useragent = new LWP::UserAgent;
$useragent->agent('Mozilla/4.0 (compatible; MSIE 5.0; Windows 95)');
$request = HTTP::Request->new('GET');
$request->url($searchurl);
$response = $useragent->request($request);
$body = $response->content;
$parsed = HTML::Parse::parse_html($body);
#The lines above seem to be setting the varibles for the continuation of the script below
#much of the script seems to be setting up for a small loop in order to properly identify everything
#that it is required to parse
for (@{ $parsed->extract_links(qw(a)) }) {
      ($link) = @$_;
      if ($link =~ m/url/){
         print $link . "\n";
         }
    } 

#these are the actual lines that get everything done and print out the required information, it gathers the links and prints them out

